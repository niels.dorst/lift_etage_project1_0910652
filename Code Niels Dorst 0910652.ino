/*
Project: project1 TI
programname: verdieping_proto.ino
author: Niels Dorst, 0910652, TI1C
purpose: control a single floor of the elevator project
date of creation: 20180914 
version history
20180914: created code, set data directions and added functionality for the shiftregister 
20180919: added functionality for the IR sensor to interact with the 7segment
20180924: added led buttons
20180926: cleaned up integration and added more comments 
*/
#define IR 2//definieren van alle pin nummers met de bijbehorende namen
#define ledFloor 3
#define datapin 4
#define latchpin 5
#define clockpin 6
#define buttonUp 7
#define buttonDwn 8
#define ledUp 9
#define ledDwn 10

int floorNumber = 0;// nummer van de verdieping wordt verkregen van de master
int oldIRState = 1;//oude status van de IR sensor
int IRState = 1;// huidige status van de IR sensor
int oldButtonStateDwn = 0;// Oude status van de knop voor omlaag
int oldButtonStateUp = 0;//Oude status van de knop voor omhoog
int buttonStateUp = 0;//status van de knop voor omhoog 
int buttonStateDwn = 0;//status van de knop voor omlaag

void setup() 
{
  pinMode(IR, INPUT);//alle pinnen definieren als input of output
  pinMode(ledFloor, OUTPUT);
  pinMode(datapign, OUTPUT);
  pinMode(latchpin, OUTPUT);
  pinMode(clockping, OUTPUT);
  pinMode(buttonUp, INPUT);
  pinMode(buttonDwn, INPUT);
  pinMode(ledUp, OUTPUT);
  pinMode(ledDwn, OUTPUT);
  Serial.begin(9600);//starten seriele monitor met een baud rate van 9600
}

void loop() 
{
  byte segmentData;
  buttonSateUp = digitalRead(buttonUp);
  buttonStateDwn = digitalRead(buttonDwn);
  IRState = digitalRead(IR);
  if(buttonStateUp != oldButtonStateUp)//if om te controleren of de knop voor omhoog is ingedrukt of losgelaten
  {
    if(buttonStateUp == HIGH)// alleen als de knop van laag naar hoog is gegaan
    {
      digitalWrite(ledUp, HIGH);//led op de knop aanzetten
      Serial.println("Lift called wants to go up")//output naar seriele monitor
    }
    delay(50);//delay voor debouncen
  }
  if(buttonStateDwn != oldButtonStateDwn)//if om te controleren of de knop voor omlaag is ingedrukt of losgelaten
  {
    if(buttonStateDwn == HIGH)//alleen las de knop van laag naar hoog is gegaan
    {
      digitalWrite(ledDwn, HIGH);//led op de knop aanzetten
      Serial.println("Lift called want to go down")//output naar seriele monitor
    }
    delay(50);//delay voor debouncen
  }
  if(IRState != OldIRState)//detecteren of de state van de ir sensor is veranderd
  {
    if(IRState == LOW)// als de ir sensor laag wordt dan gaan de leds op de knoppen uit en de led van de deur aan
    {
      digitalWrite(ledDwn,LOW);
      digitalWrite(ledUp,LOW);
      digitalWrite(ledFloor, HIGH);
      Serial.println("Lift detected doors opening")
    }
    else if(IRState == HIGH)//als de ir sensor hoog wordt gaat de led van de deur uit
    {
      digitalWrite(ledFloor, LOW);
      Serial.println("lift departed doors are closed");
    }
    delay(50);//delay voor debouncen
  }
  switch(floorNumber)//switch om de juiste data naar de shiftregister te kunnen sturen
  {
    case 0:
      segmentData = B00000011;//output van de shiftregister die correspondeert met 0 op de 7 segment
      break;
    case 1:
      segmentData = B10011111;//output van de shiftregister die correspondeert met 1 op de 7 segment
      break;
    case 2:
      segmentData = B00100101;//output van de shiftregister die correspondeert met 2 op de 7 segment
      break;
    case 3:
      segmentData = B00001101;//output van de shiftregister die correspondeert met 3 op de 7 segment
      break;
    case 4:
      segmentData = B10011001;//output van de shiftregister die correspondeert met 4 op de 7 segment
      break;
  }
  shiftRegister(segmentData);
}

void shiftRegister(byte data)//functie om de juiste data naar de shiftregister te sturen om de 7segment display aan te sturen
{
  digitalWrite(latchpin, LOW);//zorgen dat het shift register data kan ontvangen
  shiftOut(datapin, clockpin, LSBFIRST, data);//functie om de data met de data pin en de clock pin met de least significant bit eerst te sturen naar de shiftregister
  digitalWrite(latchping, HIGH);//zorgen dat het shift register geen data meer kan ontvangen
}